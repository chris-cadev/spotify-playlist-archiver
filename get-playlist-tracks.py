import json
import os
import click
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials


def extract_playlist_id(uri: str):
    return os.path.split(uri)[1]


def normalize_item(item: dict):
    # $.track.name = name
    # $.track.album.artists[n].name = artists[n].name
    # $.track.album.artists[n].type = artists[n].type
    # $.track.href = url
    track = item.get('track', {})
    name = track.get('name', None)
    url = track.get('href', None)
    artists = []
    for artist in track.get('artists', []):
        artists.append({
            "name": artist.get("name", None),
            "type": artist.get("type", None)
        })
    return {
        "name": name,
        "artists": artists,
        "url": url
    }


@click.command()
@click.option("--playlist-uri", help="playlist uri from spotify")
def command(playlist_uri):
    playlist_id = extract_playlist_id(playlist_uri)
    spotify = spotipy.Spotify(
        client_credentials_manager=SpotifyClientCredentials()
    )

    playlist = spotify.user_playlist_tracks(
        user=os.getenv("SPOTIFY_USER"),
        playlist_id=playlist_id,
        limit=200
    )
    tracks = []
    for item in playlist['tracks']['items']:
        n_item = normalize_item(item)
        tracks.append(n_item)

    playlist_name = playlist.get('name')
    playlist_sort_id = playlist_id.split("?")[0]
    name = playlist_sort_id if playlist_name == None or playlist_name == '' else playlist_name
    filename = f'playlists/{playlist_sort_id}.json'
    if playlist_sort_id != name:
        filename = f'playlists/{name} [{playlist_sort_id}].json'
    with open(filename, "w") as f:
        f.write(json.dumps({
            "name": playlist_name,
            "description": playlist.get('description'),
            "tracks": tracks
        }))
        print(filename)


if __name__ == "__main__":
    command()
