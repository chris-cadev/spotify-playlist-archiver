#!/bin/bash

for url in `cat $(dirname $0)/playlists.txt`
do
    bash $(dirname $0)/download-playlist.sh "$url"
done