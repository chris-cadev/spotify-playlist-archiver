#!/bin/bash

date_stamp() {
    echo "[`date +'%Y-%m-%d %H:%M:%S'`]"
}

mkdir playlists

playlist_uri=$1
playlist_json="`pipenv run python get-playlist-tracks.py --playlist-uri "$playlist_uri"`"
playlist_downloads_dir="`dirname "$playlist_json"`/`basename -- "$playlist_json" ".json"`"
playlist_name=`jq -c '.name' "$playlist_json"`
playlist_log_file="$playlist_downloads_dir.log"

mkdir -p "$playlist_downloads_dir"

echo "`date_stamp`: downloading '$playlist_name' playlist..." >> "$playlist_log_file"
jq -c '.tracks[]' "$playlist_json" | while read i; do
    name=`echo $i | jq '.name'`
    artist=`echo $i | jq '.artists[0].name'`
    echo "`date_stamp`: downloading "$name $artist"..." >> "$playlist_log_file"
    yt-dlp  -f 'ba' -x --audio-format mp3 ytsearch15:"$name $artist" --max-downloads 1 -o "$playlist_downloads_dir/%(title)s (%(id)s).mp3" >> "$playlist_log_file"
    echo "`date_stamp`: "$name $artist" finished!" >> "$playlist_log_file"
done

echo "`date_stamp`: '$playlist_name' downloaded!" >> "$playlist_log_file"

echo "`date_stamp`: archiving '$playlist_name' playlist..." >> "$playlist_log_file"
bash $(dirname $0)/archive-playlist.sh "$playlist_downloads_dir" "$playlist_json"
echo "`date_stamp`: '$playlist_name' archived..." >> "$playlist_log_file"