#!/bin/bash

playlist_downloads_dir=`realpath "$1"`
playlist_json=`realpath "$2"`

mv "$playlist_json" "$playlist_downloads_dir"
cd "$playlist_downloads_dir"
tar -cvzf "`realpath "$playlist_downloads_dir"`.tar.gz" "."
rm -rf "$playlist_downloads_dir"
